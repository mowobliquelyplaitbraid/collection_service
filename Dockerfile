FROM python:3.10.5-alpine3.15

WORKDIR /

COPY app.py /
COPY requirements.txt /

ENV PORT=port
ENV HOST=ip

RUN python -m pip install --upgrade pip && pip install -r requirements.txt
CMD ["sh", "-c", "flask init"]
CMD ["sh", "-c", "flask migrate"]
CMD ["sh", "-c", "flask upgrade"]
CMD ["sh", "-c", "flask run --host=$HOST --port=$PORT"]
