-- Creation of service db
CREATE DATABASE service
    WITH
    OWNER = user
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;


GRANT ALL ON DATABASE service TO user;

\c service
-- Creation of ips table
CREATE TABLE IF NOT EXISTS public.ips
(
    node character varying COLLATE pg_catalog."default" UNIQUE,
    ipmi_address inet,
    dhcp_address inet NOT NULL,
    CONSTRAINT ips_pkey PRIMARY KEY (dhcp_address)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.ips
    OWNER to user;

-- Creation of dmidecode table
CREATE TABLE IF NOT EXISTS public.dmidecode_results
(
    node character varying COLLATE pg_catalog."default" NOT NULL,
    dmidecode json,
    CONSTRAINT dmidecode_pkey PRIMARY KEY (node)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.dmidecode_results
    OWNER to user;
    
-- Creation of ipmitool table
CREATE TABLE IF NOT EXISTS public.ipmitool_results
(
    node character varying COLLATE pg_catalog."default" NOT NULL,
    ipmitool json,
    CONSTRAINT ipmitool_pkey PRIMARY KEY (node)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.ipmitool_results
    OWNER to user;
    
-- Creation of lshw table
CREATE TABLE IF NOT EXISTS public.lshw_results
(
    node character varying COLLATE pg_catalog."default" NOT NULL,
    lshw json,
    CONSTRAINT lshw_pkey PRIMARY KEY (node)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.lshw_results
    OWNER to user;
    
-- Creation of smartctl table
CREATE TABLE IF NOT EXISTS public.smartctl_results
(
    node character varying COLLATE pg_catalog."default" NOT NULL,
    smartctl json,
    CONSTRAINT smartctl_pkey PRIMARY KEY (node)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.smartctl_results
    OWNER to user;
    
-- Creation of lldp table
CREATE TABLE IF NOT EXISTS public.lldp_results
(
    node character varying COLLATE pg_catalog."default" NOT NULL,
    lldp json,
    CONSTRAINT lldp_pkey PRIMARY KEY (node)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.lldp_results
    OWNER to user;
