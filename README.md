# service for collection info from hosts

Simple module for receiving utility outputs from different nodes.


## endpoints

Has separate endpoint for every utility, saves results to the database and returns the latest result on request

## /ips:

```
http://ip:port/ips
```

GET - returns all entries from ips table.

POST - enters utility results for node (ipmi-address and node's name from netbox) and the ip from which the request has been made. Format:

{
    "ipmi_address": "value"
}

ips table has the following fields:

node - string (unique);<br />
ipmi_address - inet;<br />
dhcp_address - inet (PK);<br />

## /scan_result:

```
http://ip:port/scan_result?scanner_type=value&node=nodename, where value is one of ['dmidecode', 'ipmitool', 'lshw', 'smartctl', 'lldp'], node must be set only for GET-request
```

GET - returns the last specified utility result for the node.

POST - gets node's name from ips table by the dhcp-address from which the request has been made, writes date and time as a key to the json, and adds this json to others for this node. As input can reseive any json with an utility result.

Existing tables for each utility:

dmidecode_results<br />
ipmitool_results<br />
lldp_results<br />
lshw_results<br />
smartctl_results


Each table has the following fields:

node - string (PK);<br />
scanner_name - json;
