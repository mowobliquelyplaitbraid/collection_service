#Simple module for receiving utility outputs from different nodes.
#Has separate endpoint for every utility, saves results to the database
#and returns the latest result on request

import os
from datetime import datetime

import requests
from flask import Flask, request, abort
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects import postgresql
from werkzeug import local

POSTGRES_URI = os.environ.get('PGDB_URI')
NETBOX_URL = os.environ.get('NETBOX_URL')
NETBOX_TOKEN = (
    os.environ.get('NETBOX_TOKEN')
    or open(os.environ.get('NETBOX_TOKEN_FILE')).read().strip()
)
print(f'NETBOX_URL: {NETBOX_URL}; NETBOX_TOKEN: {NETBOX_TOKEN}')

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = POSTGRES_URI
db = SQLAlchemy(app)
migrate = Migrate(app, db)

#Classes for every DB table
class IpsModel(db.Model):
    __tablename__ = 'ips'

    node = db.Column(db.String())
    ipmi_address = db.Column(postgresql.INET, unique=True)
    dhcp_address = db.Column(postgresql.INET, unique=True, primary_key=True)

    def __init__(self, node, ipmi_address, dhcp_address):
        self.node = node
        self.ipmi_address = ipmi_address
        self.dhcp_address = dhcp_address

    def __repr__(self):
        return f'<Node {self.node}>'


class DmidecodeResultModel(db.Model):
    __tablename__ = 'dmidecode_results'

    node = db.Column(db.String(), primary_key=True)
    dmidecode = db.Column(postgresql.JSON, default={})

    def __init__(self, node):
        self.node = node

    def __repr__(self):
        return f'<Dmidecode results for node {self.node}>'


class IpmitoolResultModel(db.Model):
    __tablename__ = 'ipmitool_results'

    node = db.Column(db.String(), primary_key=True)
    ipmitool = db.Column(postgresql.JSON, default={})

    def __init__(self, node):
        self.node = node

    def __repr__(self):
        return f'<Ipmitool results for node {self.node}>'


class LshwResultModel(db.Model):
    __tablename__ = 'lshw_results'

    node = db.Column(db.String(), primary_key=True)
    lshw = db.Column(postgresql.JSON, default={})

    def __init__(self, node):
        self.node = node

    def __repr__(self):
        return f'<Lshw results for node {self.node}>'


class SmartctlResultModel(db.Model):
    __tablename__ = 'smartctl_results'

    node = db.Column(db.String(), primary_key=True)
    smartctl = db.Column(postgresql.JSON, default={})

    def __init__(self, node):
        self.node = node

    def __repr__(self):
        return f'<Smartctl results for node {self.node}>'


class LldpResultModel(db.Model):
    __tablename__ = 'lldp_results'

    node = db.Column(db.String(), primary_key=True)
    lldp = db.Column(postgresql.JSON, default={})

    def __init__(self, node):
        self.node = node

    def __repr__(self):
        return f'<Lldp results for node {self.node}>'


def push_node_ip_association_in_db(client_ip: str, request: local.LocalProxy):
    """Pushes the asssosiation node-ipmi_address-dhcp_address to the ips table.

    Args:
        client_ip (str): client dhcp address
        request (local.LocalProxy): request object

    Returns:
        str: resulting message
    """
    if request.is_json:

        data = request.get_json()
        nodename = get_nodename_by_ipmi_addr(data['ipmi_address'])
        delete_node_from_ips_if_exists(nodename, client_ip)
        current_record = IpsModel(node=nodename, ipmi_address=data['ipmi_address'], dhcp_address=client_ip)
        db.session.add(current_record)
        db.session.commit()
        message = {'message': f'address {current_record.dhcp_address} has been created successfully, '
                              f'node {current_record.node} has this address'}
    else:
        message = {'error': 'The request payload is not in JSON format'}

    return message


def get_node_ip_association_from_db():
    """Gets all ips DB entries.

    Returns:
        dict: ips DB entries
    """
    message = {}

    for ip in IpsModel.query.all():
        message[ip.dhcp_address] = ip.node

    return message


def push_scanner_data_in_db(client_nodename: str, scanner_type: str, request: local.LocalProxy):
    """Pushes utility output to the utility table.

    Args:
        client_nodename (str): name of the node from which the utility output came
        scanner_type (str): utility name
        request (local.LocalProxy): request object

    Returns:
        str: resulting message
    """
    if request.is_json:

        date_string = str(datetime.utcnow())
        data = {date_string: request.get_json()}


        if get_scan_results_from_db_if_exist(client_nodename, scanner_type) is None:
            if scanner_type == 'dmidecode':
                new_result = DmidecodeResultModel(client_nodename)
            elif scanner_type == 'ipmitool':
                new_result = IpmitoolResultModel(client_nodename)
            elif scanner_type == 'lshw':
                new_result = LshwResultModel(client_nodename)
            elif scanner_type == 'smartctl':
                new_result = SmartctlResultModel(client_nodename)
            elif scanner_type == 'lldp':
                new_result = LldpResultModel(client_nodename)
            setattr(new_result, scanner_type, data)
        else:
            if scanner_type == 'dmidecode':
                new_result = DmidecodeResultModel.query.get(client_nodename)
            elif scanner_type == 'ipmitool':
                new_result = IpmitoolResultModel.query.get(client_nodename)
            elif scanner_type == 'lshw':
                new_result = LshwResultModel.query.get(client_nodename)
            elif scanner_type == 'smartctl':
                new_result = SmartctlResultModel.query.get(client_nodename)
            elif scanner_type == 'lldp':
                new_result = LldpResultModel.query.get(client_nodename)
            current_result = dict(getattr(new_result, scanner_type))
            current_result.update(data)
            setattr(new_result, scanner_type, current_result)
        message = {'message': f'scan result for {new_result.node} has been created successfully, '
                                f'scanner type is {scanner_type}'}
        db.session.add(new_result)
        db.session.commit()
            
    else:
        message = {'error': 'The request payload is not in JSON format'}

    return message


def get_last_scan_result(nodename: str, scanner_type: str):
    """Gets the last scan result for the specified node and utility.

    Args:
        nodename (str): name of the node from request
        scanner_type (str): utility name

    Returns:
        dict: utility output for the specified node
    """
    if not nodename:
            return {'error': 'No node specified'}

    scan_results = get_scan_results_from_db_if_exist(nodename, scanner_type)
    
    if scan_results is None:
        abort(404)
    else:
        last_scan_result = getattr(scan_results, scanner_type)
        if last_scan_result != {}:
            max_date = list(last_scan_result)[0]
            for date in last_scan_result:
                if date > max_date:
                    max_date = date
            return {max_date: last_scan_result[max_date]}
        return last_scan_result


def get_scan_results_from_db_if_exist(nodename: str, scanner_type: str):
    """A help function to search if any entry for the specified node and utility exist.

    Args:
        nodename (str): name of the node from request
        scanner_type (str): utility name

    Returns:
        dict: None if no entry; first entry if exist
    """
    if scanner_type == 'dmidecode':
        scan_results = DmidecodeResultModel.query.filter(DmidecodeResultModel.node == nodename).first()
    elif scanner_type == 'ipmitool':
        scan_results = IpmitoolResultModel.query.filter(IpmitoolResultModel.node == nodename).first()
    elif scanner_type == 'lshw':
        scan_results = LshwResultModel.query.filter(LshwResultModel.node == nodename).first()
    elif scanner_type == 'smartctl':
        scan_results = SmartctlResultModel.query.filter(SmartctlResultModel.node == nodename).first()
    elif scanner_type == 'lldp':
        scan_results = LldpResultModel.query.filter(LldpResultModel.node == nodename).first()

    if scan_results:
        return scan_results

    return None


def is_scanner_type_valid(scanner_type: str):
    """Checks if scanner type is one of defined.

    Args:
        scanner_type (str): utility name

    Returns:
        str: unified utility name
    """
    valid_scanner_types = ('dmidecode', 'ipmitool', 'lshw', 'smartctl', 'lldp')

    return scanner_type.lower() in valid_scanner_types


def get_nodename_by_ipmi_addr(ipmi_address: str):
    """Finds the node name by ipmi address in Netbox.

    Args:
        ipmi_address (str): node ipmi address

    Returns:
        str: node name found by ipmi address
    """
    ipaddr_query_endpoint = NETBOX_URL + '/api/ipam/ip-addresses/?q='
    s = requests.Session()
    s.headers.update({'Authorization': 'Token ' + NETBOX_TOKEN})
    response = s.get(ipaddr_query_endpoint + ipmi_address)
    response.raise_for_status()

    return response.json()['results'][0]['assigned_object']['device']['name']


def get_node_from_ips_by_dhcp_address(dhcp_address: str):
    """Gets node entry from ips table by dhcp address.

    Args:
        dhcp_address (str): node dhcp address

    Returns:
        dict: node entry
    """
    return IpsModel.query.get(dhcp_address)


def get_node_from_ips_by_nodename(nodename: str):
    """Gets first node entry from ips table by node name.

    Args:
        nodename (str): node name

    Returns:
        dict: node entry
    """
    return IpsModel.query.filter(IpsModel.node == nodename).first()


def delete_node_from_ips_if_exists(nodename: str, client_ip: str):
    """Deletes node entry from ips table if it can be found by dhcp address or node name.

    Args:
        nodename (str): node name
        client_ip (str): dhcp address
    """
    if get_node_from_ips_by_dhcp_address(client_ip):
        db.session.delete(get_node_from_ips_by_dhcp_address(client_ip))
    if get_node_from_ips_by_nodename(nodename):
        db.session.delete(get_node_from_ips_by_nodename(nodename))
    db.session.commit()


@app.route('/ips', methods=['POST', 'GET'])
def handle_ips():
    """Endpoint to push and get entries to the ips table.

    Returns:
        str: resulting message
    """
    client_ip = request.remote_addr
    
    if request.method == 'POST':
        return push_node_ip_association_in_db(client_ip, request)

    elif request.method == 'GET':
        return get_node_ip_association_from_db()


@app.route('/scan_result', methods=['POST', 'GET'])
def handle_scanners():
    """Endpoint to push and get entries to utility tables.

    Returns:
        str: resulting message
    """

    scanner_type = request.args.get('scanner_type', default='', type=str)
    requested_nodename = request.args.get('node', type=str)
    
    if not is_scanner_type_valid(scanner_type):
        return {'error': 'The request parameter scanner_type is invalid'}

    if request.method == 'POST':
        node = get_node_from_ips_by_dhcp_address(request.remote_addr)
        if not node:
            return {'error': 'No node with this dhcp address in ips table'}
        return push_scanner_data_in_db(node.node, scanner_type, request)

    elif request.method == 'GET':
        return get_last_scan_result(requested_nodename, scanner_type)



if __name__ == '__main__':
    app.run(debug=True)
